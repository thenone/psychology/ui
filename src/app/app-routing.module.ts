import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { LoginComponent } from './shell/pages/login/login.component';
import { NotFoundComponent } from './shell/pages/not-found/not-found.component';

// Guards
import { CanActivateLoginGuard } from './shared/guards/can-activate-login.guard';
import { CanLoadAdminGuard } from './shared/guards/can-load-admin.guard';

const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent, canActivate: [CanActivateLoginGuard] },
    { path: 'admin', loadChildren: './admin/admin.module#AdminModule', canLoad: [CanLoadAdminGuard] },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
