import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../../../shared/services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class CanActivateAdminChildGuard implements CanActivate {

    constructor(private router: Router, private auth: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.auth.isLoggedIn() && this.auth.isAdmin() && this.auth.getTokenExp() > new Date()) {
            return true;
        }
        this.router.navigate(['/login']).then(() => { console.log('Access denied. Log in first'); });
    }
}
