import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { UsersService } from '../../shared/services/users.service';
import { User } from '../users/user';

@Component({
    selector: 'app-patients',
    templateUrl: './patients.component.html',
    styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {

    constructor(private users: UsersService) {
        this.searchForm = new FormGroup({
            national_id: new FormControl()
        });
    }

    patients: User[];
    searchForm: FormGroup;

    ngOnInit() {
        this.users.getPatients(10).subscribe(value => this.patients = <User[]>value);
    }

    public onSubmit() {
        this.users.searchPatients({ national_id: this.searchForm.controls.national_id.value })
            .subscribe(value => this.patients = <User[]>value);
    }
}
