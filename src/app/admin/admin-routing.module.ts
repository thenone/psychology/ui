import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { SidebarComponent } from '../shared/components/sidebar/sidebar.component';
import { NewUsersComponent } from './users/new-users/new-users.component';
import { BannedUsersComponent } from './users/banned-users/banned-users.component';
import { PatientsComponent } from './patients/patients.component';
import { QuestionsComponent } from './questions/questions.component';
import { AnswersComponent } from './answers/answers.component';

// Guards
import { CanActivateAdminChildGuard } from './shared/guards/can-activate-admin-child.guard';

const routes: Routes = [
    { path: '', component: SidebarComponent, outlet: 'sidebar', },
    { path: 'new-users', component: NewUsersComponent, canActivate: [CanActivateAdminChildGuard] },
    { path: 'banned-users', component: BannedUsersComponent, canActivate: [CanActivateAdminChildGuard] },
    { path: 'patients', component: PatientsComponent, canActivate: [CanActivateAdminChildGuard] },
    { path: 'patient-answers/:patient_id', component: AnswersComponent, canActivate: [CanActivateAdminChildGuard] },
    { path: 'questions', component: QuestionsComponent, canActivate: [CanActivateAdminChildGuard] },
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
