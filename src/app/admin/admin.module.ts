import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing Module
import { AdminRoutingModule } from './admin-routing.module';

// Components
import { SidebarComponent } from '../shared/components/sidebar/sidebar.component';
import { NewUsersComponent } from './users/new-users/new-users.component';
import { BannedUsersComponent } from './users/banned-users/banned-users.component';
import { PatientsComponent } from './patients/patients.component';
import { QuestionsComponent } from './questions/questions.component';

// Services
import { PersianDateService } from '../shared/services/persian-date.service';

// Pipes
import { PersianDatePipe } from '../shared/pipes/persian-date.pipe';
import { AnswersComponent } from './answers/answers.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        AdminRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [
        NewUsersComponent,
        SidebarComponent,
        PatientsComponent,
        PersianDatePipe,
        BannedUsersComponent,
        QuestionsComponent,
        AnswersComponent
    ],
    providers: [
        PersianDateService
    ]
})
export class AdminModule { }
