import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import Highcharts from 'highcharts';

declare var $;

import { UsersService } from '../../shared/services/users.service';
import { NotificationService } from '../../shared/services/notification.service';
import { QuestionsService } from '../../shared/services/questions.service';
import { PersianDateService } from '../../shared/services/persian-date.service';

import { User } from '../users/user';
import { Answer } from '../../shared/models/answer';


@Component({
    selector: 'app-answers',
    templateUrl: './answers.component.html',
    styleUrls: ['./answers.component.css']
})
export class AnswersComponent implements OnInit {

    constructor(
        private router: ActivatedRoute,
        private users: UsersService,
        private questions: QuestionsService,
        private notify: NotificationService,
        private persianDate: PersianDateService) { }

    userID: string;
    user: User;
    qID: number;
    s;
    buttonDisabled = true;

    ngOnInit() {
        this.userID = this.router.snapshot.paramMap.get('patient_id');
        this.qID = parseInt(this.router.snapshot.queryParamMap.get('question'), 10) || 1;
        this.users.getUser(this.userID).subscribe(
            value => {
                this.user = value;
                this.onSuccessUserFetch(this.user);
            },
            error => this.onUserFetchError(error)
        );
        this.s = $('.js-example-basic-single').select2({ width: 'resolve' });
    }

    private onSuccessUserFetch(user: User) {
        this.buttonDisabled = false;
        this.user = user;
        this.getAnswers();
    }

    getAnswers() {
        this.qID = parseInt(this.s[0].value, 10);
        this.questions.getUserAnswers(this.user._id, this.qID).subscribe(
            value => {
                if (value.length === 0) {
                    const container = $('#container');
                    container.html(`
                            <div>
                                <h2 style="text-align: center;">کاربر به سوال ${this.qID} تا کنون پاسخی نداده است</h2>
                            </div>`);
                    container.css('margin-bottom', '7em');
                } else {
                    $('#container').css('margin-bottom', '1em');
                    this.drawChart(value);
                }
            }
        );
    }

    private onUserFetchError(error: HttpErrorResponse) {
        this.notify.error('کاربر وجود ندارد');
    }

    private drawChart(value: Answer[]) {
        const series: {
            data: number[];
            name: string;
        }[] = [{
            data: [],
            name: ''
        }];
        let xAxis: string[];
        if (this.qID === 1 || this.qID === 2 || this.qID === 6) {
            const res = this.getFirstLevelAnswersDetails(value);
            series[0] = {
                data: res.selected_options,
                name: 'گزینه انتخابی'
            };
            xAxis = res.dates;

        } else if (this.qID === 3 || this.qID === 5) {
            const res = this.getSecondLevelAnswers(value);
            series[0] = { data: res.selected_options, name: 'انتخاب اول' };
            series[1] = { data: res.secondary_selected_options, name: 'انتخاب دوم' };
            xAxis = res.dates;
        }

        const chart = Highcharts.chart('container', {
            title: {
                text: `پاسخ‌های ${this.user.first_name} ${this.user.last_name}`
            },
            chart: {
                type: 'line',
                style: {
                    fontFamily: 'BRoya',
                    fontSize: 'larger'
                }
            },
            xAxis: {
                categories: xAxis
            },
            yAxis: [{
                title: {
                    text: 'پاسخ انتخابی'
                },
                gridLineWidth: 0
            }],
            legend: {
                useHTML: true,
                rtl: true,
                layout: 'vertical',
                backgroundColor: '#FFFFFF',
                floating: true,
                align: 'left',
                x: 100,
                verticalAlign: 'top',
                y: 70
            },
            tooltip: {
                formatter: function () {
                    if (this.y === 0 || this.y === 1) {
                        if (this.y === 0) {
                            return `${this.series.name}: ${'خیر'} در ${this.x}`;
                        } else {
                            return `${this.series.name}: ${'بله'} در ${this.x}`;
                        }
                    } else {
                        return `${this.series.name}: ${this.y} در ${this.x}`;
                    }
                }
            },
            plotOptions: {},
            series: series
        });
    }

    private getFirstLevelAnswersDetails(answers: Answer[]): { selected_options: number[], dates: string[] } {
        const selected_options: number[] = [];
        const dates: string[] = [];
        answers.forEach(answer => {
            selected_options.push(answer.selected_option);
            dates.push(this.persianDate.toPersian(answer.answered_date, 'M/D'));
        });
        return {
            selected_options: selected_options,
            dates: dates
        };
    }

    private getSecondLevelAnswers(answers: Answer[]): {
        selected_options: number[],
        secondary_selected_options: number[],
        dates: string[]
    } {
        const selected_options: number[] = [];
        const secondary_selected_options: number[] = [];
        const dates: string[] = [];
        answers.forEach(answer => {
            selected_options.push(answer.selected_option);
            secondary_selected_options.push((answer.secondary_selected_option + 1) || 0);
            dates.push(this.persianDate.toPersian(answer.answered_date, 'M/D'));
        });
        return {
            selected_options: selected_options,
            secondary_selected_options: secondary_selected_options,
            dates: dates
        };
    }
}
