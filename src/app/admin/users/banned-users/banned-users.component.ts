import { Component, OnInit } from '@angular/core';

import { UsersService } from '../../../shared/services/users.service';
import { NotificationService } from '../../../shared/services/notification.service';

import { User } from '../user';
import { Config } from '../../../config.env';

@Component({
    selector: 'app-banned-users',
    templateUrl: './banned-users.component.html',
    styleUrls: ['./banned-users.component.css']
})
export class BannedUsersComponent implements OnInit {

    constructor(private users: UsersService, private notify: NotificationService) { }

    bannedUsers: User[];

    ngOnInit() {
        this.users.getBannedUsers(10).subscribe(value => this.bannedUsers = value);
    }

    restoreUser(userID: string) {
        this.users.changeUserStatus(userID, Config.status.unassigned).subscribe(
            value => {
                console.log('user rejected successfully', value);
                this.notify.success('بازگردانی کاربر با موقیت انجام شد');
                this.ngOnInit();
            });
    }
}
