export class User {
    _id: string;
    first_name: string;
    last_name: string;
    father_name?: string;
    national_id: number;
    mobile: number;
    date_registered: Date;
    status: number;
}
