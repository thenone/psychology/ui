import { Component, OnInit } from '@angular/core';

import { UsersService } from '../../../shared/services/users.service';
import { NotificationService } from '../../../shared/services/notification.service';

import { User } from '../user';
import { Config } from '../../../config.env';

@Component({
    selector: 'app-new-users',
    templateUrl: './new-users.component.html',
    styleUrls: ['./new-users.component.css']
})
export class NewUsersComponent implements OnInit {

    constructor(private usersService: UsersService, private notify: NotificationService) { }

    users: User[];

    ngOnInit() {
        this.usersService.getNewUsers(20, 0).subscribe(
            u => {
                this.users = u as User[];
            },
            err => {
                if (err) { console.log(err); }
            }
        );
    }

    acceptUser(userID: string) {
        console.log('accept user called. user id:', userID);
        this.usersService.changeUserStatus(userID, Config.status.accepted).subscribe(
            value => {
                console.log('user accepted successfully', value);
                this.notify.success('ثبت کاربر با موفقیت انجام شد');
                this.ngOnInit();
            }
        );
    }

    rejectUser(userID: string) {
        if (confirm('are you sure?')) {
            console.log('reject user called. user id:', userID);
        }
        this.usersService.changeUserStatus(userID, Config.status.banned).subscribe(
            value => {
                console.log('user rejected successfully', value);
                this.notify.success('حذف کاربر با موقیت انجام شد');
                this.ngOnInit();
            });
    }
}
