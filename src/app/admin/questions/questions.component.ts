import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

import { QuestionsService } from '../../shared/services/questions.service';

import { Question } from '../../shared/models/question';

@Component({
    selector: 'app-questions',
    templateUrl: './questions.component.html',
    styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

    constructor(private questionService: QuestionsService) { }

    questions: Question[];
    selectedItem = 0;

    ngOnInit() {
        this.questionService.getList(10).subscribe(
            value => {
                this.questions = value;
            }
        );
    }

    toggleItem(itemID: number) {
        const clickedItemBody = $(`#${itemID} .question--item-body`);
        const restItemsBody = $(`.question--item .question--item-body`);
        if (this.selectedItem === itemID) {
            clickedItemBody.slideToggle('slow');
        } else {
            restItemsBody.slideUp('slow');
            clickedItemBody.slideDown('slow');
        }
        this.selectedItem = itemID;
    }
}
