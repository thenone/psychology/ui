import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthService } from '../../../shared/services/auth.service';
import { FormValidatorService } from '../../../shared/services/form-validator.service';
import { NotificationService } from '../../../shared/services/notification.service';

import { SuccessLogin } from '../../../shared/models/LoginResponse';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    constructor(
        private auth: AuthService,
        private router: Router,
        private notify: NotificationService
    ) {
        this.createForm();
    }

    submitButtonEnabled = true;
    loginForm: FormGroup;

    private createForm() {
        this.loginForm = new FormGroup({
            username: new FormControl('09', [Validators.required, FormValidatorService.mobile]),
            password: new FormControl('', [Validators.required])
        });
    }

    public onSubmit() {
        this.submitButtonEnabled = false;
        this.auth.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value).subscribe(
            res => this.onSuccess(res as SuccessLogin),
            error => this.onError(error),
            () => this.submitButtonEnabled = true
        );
    }

    private onSuccess(res: SuccessLogin) {
        this.notify.success('خوش آمدید!');
        this.auth.setLogin(res);
        if (this.auth.isAdmin()) {
            this.router.navigate(['/admin']).then(() => { console.log('Welcome Admin!'); });
        } else {
            this.notify.error('باید مدیر باشید');
        }
    }

    private onError(error: HttpErrorResponse) {
        if (error.status !== 0) {
            this.notify.error('خطا', 'نام کاربری یا رمز عبور اشتباه است');
        }
    }
}
