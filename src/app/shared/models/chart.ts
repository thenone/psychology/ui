export class Chart {
    chart: {
        type: 'line';
    };
    xAxis: {
        categories: string[];
    };
    yAxis: {
        title: {
            text: string;
        };
    }[];
    legend: {
        layout: 'vertical',
        backgroundColor: '#FFFFFF',
        floating: true,
        align: 'left',
        x: 100,
        verticalAlign: 'top',
        y: 70
    };
    series: {
        data: number[];
    }[];
}
