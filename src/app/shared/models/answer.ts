export class Answer {
    _id: string;
    selected_option: number;
    secondary_selected_option?: number;
    description?: string;
    answered_date: string;
    question: string;
    user: string;
}
