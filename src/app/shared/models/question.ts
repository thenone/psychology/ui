export class Question {
    options: number[];
    options_title: string[];
    secondary_options: string[];
    secondary_options_on: number;
    created_at: Date;
    _id: string;
    title: string;
    id: number;
}
