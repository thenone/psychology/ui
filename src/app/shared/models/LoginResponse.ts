export class SuccessLogin {
    id: string;
    exp: string;
    token: string;
    roles: string[];
}

export class FailedLogin {
    message: string;
}
