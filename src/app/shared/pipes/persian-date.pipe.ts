import { Pipe, PipeTransform } from '@angular/core';

import { PersianDateService } from '../services/persian-date.service';

@Pipe({
    name: 'persianDate'
})
export class PersianDatePipe implements PipeTransform {

    constructor(private persianDate: PersianDateService) {}

    transform(ISODate: string, format = 'l'): any {
        return this.persianDate.toPersian(ISODate, format);
    }

}
