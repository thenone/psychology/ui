import { Injectable } from '@angular/core';
import { CanLoad, Route, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class CanLoadAdminGuard implements CanLoad {

    constructor(private auth: AuthService, private router: Router) { }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        if (this.auth.isLoggedIn() && this.auth.isAdmin() && this.auth.getTokenExp() > new Date()) {
            return true;
        }
        this.router.navigate(['/login']).then(() => { console.log('Log in first'); });
    }
}
