import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

import { AuthService } from '../../services/auth.service';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

    constructor(
        private auth: AuthService,
        private router: Router,
        private notify: NotificationService
    ) { }

    ngOnInit() {
        this.openNav();
        const dropdown = $('div.dropdown > a');
        $('div.dropdown > div.item.submenu').slideDown();
        $('.sidenav--items div.item:not(".dropdown") a').click(function () {
            const chosenItem = $('.sidenav--items div.item.active');
            chosenItem.removeClass('active');
            $(this).parent().addClass('active');
        });
    }

    openNav() {
        document.getElementById('sidenav').style.width = '18.75%';
        if ($('#mamad').hasClass('active')) {
            $('#taqi').slideUp();
        } else {
            $('#taqi').hide();
        }
        $('#mamad').removeClass('active');
    }

    logout() {
        this.auth.logout().subscribe(
            next => {
                this.router.navigate(['/login']).then(() => {
                    this.notify.success('خروج با موفقیت انجام شد');
                });
            }
        );
    }
}
