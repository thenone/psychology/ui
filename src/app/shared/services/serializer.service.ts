import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SerializerService {

    constructor() { }

    serialize(object: object): string {
        return Object.keys(object).filter(elem => !!object[elem]).map(key => key + '=' + object[key]).join('&');
    }
}
