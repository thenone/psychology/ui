import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from '../../admin/users/user';

import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    constructor(private http: HttpClient) { }

    public getNewUsers(limit?: number, status?: number): Observable<User[]> {
        const opts = {
            params: new HttpParams().set('limit', limit.toString()).set('status', status.toString())
        };
        return this.http.get<User[]>(`${environment.baseURL}/users`, opts);
    }

    public getPatients(limit: number): Observable<User[]> {
        const opts = {
            params: new HttpParams().set('limit', limit.toString()).set('status', '1')
        };
        return this.http.get<User[]>(`${environment.baseURL}/users`, opts);
    }

    public searchPatients(params: { national_id: '' }): Observable<User[]> {
        const opts = {
            params: new HttpParams().set('national_id', params.national_id.toString())
        };
        return this.http.get<User[]>(`${environment.baseURL}/users`, opts);
    }

    public changeUserStatus(id: string, status: number): Observable<null> {
        return this.http.put<null>(`${environment.baseURL}/users/${id}/status/${status}`, null);
    }

    public getBannedUsers(limit: number): Observable<User[]> {
        const opts = {
            params: new HttpParams().set('limit', limit.toString()).set('status', '2')
        };
        return this.http.get<User[]>(`${environment.baseURL}/users`, opts);
    }

    public getUser(id: string): Observable<User> {
        return this.http.get<User>(`${environment.baseURL}/users/${id}`);
    }
}
