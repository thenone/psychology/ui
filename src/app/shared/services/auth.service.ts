import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { SerializerService } from './serializer.service';
import { SuccessLogin } from '../models/LoginResponse';

import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private serializer: SerializerService,
        private http: HttpClient
    ) { }

    login(mobile: string, password: string) {
        const form = {
            username: mobile.substr(1),
            password: password
        };
        return this.http.post(environment.baseURL + '/auth/login', this.serializer.serialize(form));
    }

    setLogin(login: SuccessLogin) {
        this.setToken(login.token);
        if (login.roles.length === 1 && login.roles.indexOf('ROLE_ADMIN') === 0) {
            this.setIsAdmin(true);
        } else {
            this.setIsAdmin(false);
        }
        localStorage.setItem('psycho_token_exp', login.exp);
    }

    getTokenExp(): Date {
        return new Date(localStorage.getItem('psycho_token_exp'));
    }

    logout() {
        localStorage.removeItem('auth_token');
        return of(true);
    }

    private setToken(token: string) {
        localStorage.setItem('auth_token', token);
    }

    private setIsAdmin(isAdmin: boolean) {
        localStorage.setItem('psycho_is_admin', isAdmin ? '1' : '0');
    }

    public isAdmin(): boolean {
        return localStorage.getItem('psycho_is_admin') === '1';
    }

    getToken(): string {
        return localStorage.getItem('auth_token');
    }

    isLoggedIn(): boolean {
        return !!this.getToken();
    }
}
