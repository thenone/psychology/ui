import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Question } from '../models/question';
import { Answer } from '../models/answer';

import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class QuestionsService {

    constructor(private http: HttpClient) { }

    getList(limit: number = 5): Observable<Question[]> {
        const opts = {
            params: new HttpParams().set('limit', limit.toString())
        };
        return this.http.get<Question[]>(`${environment.baseURL}/questions`, opts);
    }

    getUserAnswers(userID: string, questionID: number): Observable<Answer[]> {
        return this.http.get<Answer[]>(`${environment.baseURL}/users/${userID}/answers?question=${questionID}`);
    }
}
