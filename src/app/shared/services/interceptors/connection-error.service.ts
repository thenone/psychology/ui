import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {filter, tap} from 'rxjs/operators';

import { NotificationService } from '../notification.service';


@Injectable({
    providedIn: 'root'
})
export class ConnectionErrorService implements HttpInterceptor {

    constructor(private notify: NotificationService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap(
                event => { },
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        if (err.status === 0) {
                            this.notify.error('خطا', 'اتصال اینترنت خود را بررسی کنید');
                        }
                    }
                }
            )
        );
    }

}
