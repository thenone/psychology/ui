import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ContentTypeInterceptorService implements HttpInterceptor {

    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.method === 'GET' || req.method === 'DELETE') {
            return next.handle(req);
        }
        req = req.clone({
            setHeaders: {
                'Content-Type': `application/x-www-form-urlencoded`
            }
        });
        return next.handle(req);
    }
}
