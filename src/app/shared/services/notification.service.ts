import { Injectable } from '@angular/core';

// NotifyJs jQuery selector
declare let $: any;


@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor() { }

    success(...messages: string[]) {
        $.notify(messages.join(' - '), {
            className: 'success',
            position: 'top-left'
        });
    }

    error(...messages: string[]) {
        $.notify(messages.join(' - '), {
            className: 'error',
            position: 'top-left'
        });
    }
}
