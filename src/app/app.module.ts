import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

// Routing Modules
import { AppRoutingModule } from './app-routing.module';

// Interceptors
import { ConnectionErrorService } from './shared/services/interceptors/connection-error.service';
import { TokenInterceptorService } from './shared/services/interceptors/token-interceptor.service';
import { ContentTypeInterceptorService } from './shared/services/interceptors/content-type-interceptor.service';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './shell/pages/login/login.component';
import { NotFoundComponent } from './shell/pages/not-found/not-found.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        NotFoundComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        ReactiveFormsModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ConnectionErrorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ContentTypeInterceptorService,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
